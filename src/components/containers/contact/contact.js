import React from 'react';
import { useTranslation } from 'react-i18next';

export const Contact = ({info}) => {
  const { t } = useTranslation();
  
  return (
    <section className="text-center imagebg" id="contact">
      <div className="container">
        <div className="row">
          <div className="col-md-8 col-lg-6">
            <div className="cta">
              <h2> {t(info.title) } </h2>
              <h4>{t(info.subtitle)}</h4>
              <h5> {t(info.buttonGroupTitle)} </h5>
              <a
                className="btn btn--primary type--uppercase"
                href={`mailto:${t(info.contactEmailSubject)}`}
              >
                <span className="btn__text"> {info.contactEmail } </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
