import facultyApp1 from  './1-dashboard.jpg';
import facultyApp2 from  './2-course-details.jpg';
import facultyApp3 from  './3-course-students.jpg';
import facultyApp4 from  './4-course-students-search.jpg';
import facultyApp5 from  './5-courses-recent.jpg';
import facultyApp6 from  './6-course-history.jpg';
import facultyApp7 from  './7-communication.jpg';
import facultyApp8 from  './8-exams-submission.jpg';
import facultyApp9 from  './9-exams-submission-2.jpg';
import facultyApp10 from  './10-exams-submission-3.jpg';
import facultyApp11 from  './11-exams-history-changes.jpg';
import facultyApp12 from  './12-exams-statistics.jpg';
import facultyApp13 from  './13-theses-completed.jpg';
import facultyApp14 from  './14-profile.jpg';
import facultyApp15 from  './15-info-department.jpg';

export default {
  facultyApp1,
  facultyApp2,
  facultyApp3,
  facultyApp4,
  facultyApp5,
  facultyApp6,
  facultyApp7,
  facultyApp8,
  facultyApp9,
  facultyApp10,
  facultyApp11,
  facultyApp12,
  facultyApp13,
  facultyApp14,
  facultyApp15
}
