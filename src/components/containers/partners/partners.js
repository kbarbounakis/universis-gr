import React from 'react';
import { ButtonLink } from './../../components/button-group/button-group';
import { useTranslation } from 'react-i18next';
import { images } from './../../../images/images';

/**
 * 
 * Presents a single partner item
 * 
 */
export const Partner = props => {
  return (
    <div className="col-md-4">
      <div className="feature feature-8 text-center">
        <img alt={props.imageAlt} src={props.image} />
        <h5>{props.title}</h5>
      </div>
    </div>
  );
}

/**
 * 
 * Holds the list of partners
 * It is expected to be used inside a PageSection component
 * 
 */
export const PartnersList = props => {
  const { t } = useTranslation();
  const partners = props.partners.map((item, i) => (
    <Partner
      key={i}
      title={t(item.title)}
      image={images[item.imageName]}
      imageAlt={t(item.imageAlt)}
    />
  ));
  
  return (
    <>
      {partners}
      <div className="col-md-4">
        <div className="feature feature-8 member-action">
          <ButtonLink text={'Sections.Universities.BePartOfTheAction'} href='#contact' />
        </div>
      </div>
    </>
  );
}

export default PartnersList;
