import React from 'react';
import { graphql } from 'gatsby';
import PropTypes from 'prop-types';
import FeatureCard from '../../components/feature-card/feature-card';
import Timeline from '../../components/timeline/timeline';
import ButtonGroup from '../../components/button-group/button-group';
import PartnerList from '../partners/partners';
import Contact from '../contact/contact';
import { useTranslation } from 'react-i18next';
import { CarouselWrapper } from './../../components/carousel/carousel';
import NewsList from "../news/news";

/**
 *
 * Maps the the children types available to the user to their system values
 *
 */
const getChildrenType = (childrenType) => {
  switch(childrenType) {
    case 'FeatureCardJustImage': return 'justImage';
    case 'FeatureCardImageFirst': return 'imageFirst';
    case 'FeatureCardWithIcon': return 'withIcon';
    case 'FeatureCardWithIconInline': return 'withIconInline';
    case 'TimelineSections': return 'timelineEntry';
    case 'Partners': return 'partners';
    case 'Contact': return 'contact';
    case 'Carousel': return 'carousel';
    case 'News': return 'news';
    default: return '';
  }
}

/**
 *
 * Populates a section with it's children
 *
 * @param {string} childrenType  The type of the section's children
 * @param {object } childrenData The data to pass to the children for rendering
 *
 */
const getSectionChildren = (childrenType, childrenData) => {
  if (!childrenData) {
    return [];
  }

  const type = getChildrenType(childrenType);
  if (type === 'timelineEntry') {
    return (
      <Timeline sections={childrenData} />
    );
  } else if (['justImage', 'imageFirst', 'withIcon', 'withIconInline'].indexOf(type) >= 0) {
    return childrenData.map((child, i) => {
      return (
        <FeatureCard
          key={i}
          featureType={type}
          {...child}
        />
      );
    });
  } else if ( type === 'partners') {
    return (
      <PartnerList partners={childrenData} />
    )
  } else if ( type === 'contact' ) {
    return <Contact childrenData={childrenData} />
  } else if ( type === 'carousel') {
    return childrenData.map((carousel, i) => {
      return <CarouselWrapper title={carousel.title} images={carousel.items} key={i} allowFullScreen/>
    });
  } else if(type === 'news'){
    return (
        <NewsList news={childrenData} />
    )
  } else {
    return undefined;
  }
}

/**
 *
 * Page section component
 *
 * The whole page is built by a list of section. This component represents a
 * single of those sections
 *
 */
export const PageSection = props => {
  const { t } = useTranslation();
  const children = getSectionChildren(props.childrenType, props.childrenData);
  let subtitle;

  if (props.subtitle && props.subtitle.indexOf('\n') > 0) {
    subtitle = [];
    props.subtitle.split('\n').forEach((chunk, i) => {
      subtitle.push(t(chunk));
      if (i < props.subtitle.length -1) {
      subtitle.push(<br key={i} />);
      }
    });
  } else {
    subtitle = t(props.subtitle);
  }

  const buttonGroup = props.buttonGroup
    ? <ButtonGroup buttons={props.buttonGroup.buttons} title={props.buttonGroup.title} />
    : undefined;

  const extraClasses = [];

  if (props.isSecondary) {
    extraClasses.push('bg--secondary');
  }

  return (
    <>
      <section className={`text-center ${extraClasses.join(' ')}`} id={props.name} >
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-md-10 col-lg-8" id="action">
              <h2>{t(props.title)}</h2>
              <p className="lead">{subtitle}</p>
            </div>
          </div>
        </div>
      </section>
      <section className={`${ props.isSecondary ? 'bg--secondary' : ''}`}>
        <div className="container">
          <div className="row">
            { children }
          </div>
          {buttonGroup}
        </div>
      </section>
    </>
  );
}

PageSection.propTypes = {
  title: PropTypes.string,
  subtitle: PropTypes.string,
  childrenType: PropTypes.string,
  children: PropTypes.array
}

export default PageSection;

export const query = graphql`
  fragment SectionsData on ContentJsonSections {
    name
    title
    subtitle
    childrenType,
    navigationLabel,
    children {
      id
      title
      body
      icon
      imageAlt,
      imageName,
      containsURL,
      items {
        imageName,
        imageAlt,
        title
        body
      }
    },
    buttonGroup {
      title,
      buttons {
        text
        href
        external
      }
    }
  }
`;
